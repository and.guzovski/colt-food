# Colt Food

Colt Food is an Online Food Ordering System made as a homework in Distributed Systems (2020) course in Tallinn University of Technology.

## Technical Part

Server side was made using .NET Core, Entity Framework, MSSQL Database.
Front-end application were made using TypeScript and Javascript frameworks.

Application has 3 separate clients:
- Customer app - [food-delivery-client](food-delivery-client), Vue.js & TypeScript
- Restaurant app - [food-delivery-restaurant](food-delivery-restaurant), Aurelia & TypeScript
- Administrator app - [FoodDeliveryApp](FoodDeliveryApp), ASP.NET Core MVC

### Requirements
- [X] Minimal 10 functional entities
- [X] Clean layered architecture including:
    - Domain Level
    - Data Access Layer (EF Core & Repository Pattern)
    - Business Logic Layer
    - REST/Web Controllers
- [X] DTO Mapping between every layer
- [X] Swagger documentation
- [X] API versioning support
- [X] Identity support (i.e. login, register, and JWT)
- [X] Use of the personal project base (must be uploaded in NuGet)
- [X] Internationalization both in ASP.NET webapp, and in REST endpoints
- [X] REST endpoint security
- [X] Docker support
- [X] Fully functional client application with usable UI/UX


